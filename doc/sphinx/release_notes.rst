Release notes
=============

Release history and changelog for the tsfpga project.

.. include:: ../../generated/sphinx/release_notes.rst
