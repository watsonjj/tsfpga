.. include:: ../../generated/sphinx/index.rst

.. toctree::
    :caption: About
    :hidden:

    license_information
    contributing
    release_notes


.. toctree::
    :caption: Technical documentation
    :hidden:

    simulation
    fpga_build
    formal
    netlist_build
    module_structure
    registers
    hdl_modules

.. toctree::
    :caption: API reference
    :hidden:

    api_reference/tsfpga
    api_reference/tsfpga.examples
    api_reference/tsfpga.tools
    api_reference/tsfpga.vivado
